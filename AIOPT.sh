#!/bin/bash
#################################
#  AllInOnePackageTracking v0.1 #
# Copyleft (ɔ) 2019 by Cooleech #
#################################
Naslov="AIOPT v0.1"
function START {
if [ "$TrackMe" = "" ]; then
 TrackMe="Upiši ovdje oznaku praćenja paketa"
fi
TrackMe=`zenity --entry --width 300 --title "$Naslov" --text "Upišite oznaku za praćenje paketa:" --entry-text "$TrackMe" --cancel-label "Izlaz" --ok-label "Uprati"`
if [ $? != 0 ]; then
 exit 1
else
 case "$TrackMe" in
 *" "*)
  zenity --error --width 250 --title "$Naslov" --text "Oznaka za praćenje ne može imati razmak!"
  START
 ;;
 "")
  zenity --warning --width 200 --title "$Naslov" --text "Niste unijeli oznaku za praćenje!\n\nIzlazim."
  exit 2
 ;;
 *)
  xdg-open https://t.17track.net/en#nums=$TrackMe
  exit 0
 ;;
 esac
fi
}

START